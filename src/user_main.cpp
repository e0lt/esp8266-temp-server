#include <iostream>
#include <string>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#define STRING(num) #num

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

/*ADD YOUR PASSWORD BELOW*/
const char *ssid = "wifi name";
const char *password = "The very secret password you tell NOBODY";
float temperatureC;

ESP8266WebServer server(80); //Server on port 80


void handleRoot() {
  Serial.println("Someone connecting");
  server.send(200, "text/plain", String(temperatureC));
}

void setup()
{
  // Start the Serial Monitor
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  int retries = 0;
  while ((WiFi.status() != WL_CONNECTED) && (retries < 15))
  {
    retries++;
    delay(500);
    Serial.print(".");
  }
  if (retries > 14)
  {
    Serial.println(F("WiFi connection FAILED"));
  }
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println(F("WiFi connected!"));
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
  
  // Wifi should reconnect, because... WIFI
  WiFi.setAutoReconnect(true);
  WiFi.persistent(true);

  Serial.println(F("Wifi setup ready"));

  // Start the DS18B20 sensor
  sensors.begin();

  server.on("/", handleRoot); //Which routine to handle at root location
  server.begin();                  //Start server
}

void loop()
{
  server.handleClient();          //Handle client requests
  sensors.requestTemperatures();
  temperatureC = sensors.getTempCByIndex(0);
  Serial.print(temperatureC);
  Serial.println("ºC");
  delay(20);
}
